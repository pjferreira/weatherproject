package com.tqs.weather;

import java.io.Serializable;

public class Currently implements Serializable
{
    private long time;
    private String summary;
    private float precipIntensity;
    private float precipProbability;
    private float temperature;
    private float apparentTemperature;
    private float dewPoint;
    private float humidity;
    private float pressure;
    private float windSpeed;
    private float windGust;
    private int windBearing;
    private float cloudCover;
    private int uvIndex;
    private float visibility;
    private float ozone;
    private long sunriseTime;
    private long sunsetTime;
    private float moonPhase;
    private float precipIntensityMax;
    private float temperatureHigh;
    private long temperatureHighTime;
    private float temperatureLow;
    private long temperatureLowTime;
    private float apparentTemperatureHigh;
    private long apparentTemperatureHighTime;
    private float apparentTemperatureLow;
    private long apparentTemperatureLowTime;
    private long windGustTime;
    private long uvIndexTime;
    private float temperatureMin;
    private long temperatureMinTime;
    private float temperatureMax;
    private long temperatureMaxTime;
    private float apparentTemperatureMin;
    private long apparentTemperatureMinTime;
    private float apparentTemperatureMax;
    private long apparentTemperatureMaxTime;
    private String icon;

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public float getPrecipIntensity()
    {
        return precipIntensity;
    }

    public void setPrecipIntensity(float precipIntensity)
    {
        this.precipIntensity = precipIntensity;
    }

    public float getPrecipProbability()
    {
        return precipProbability;
    }

    public void setPrecipProbability(float precipProbability)
    {
        this.precipProbability = precipProbability;
    }

    public float getTemperature()
    {
        return temperature;
    }

    public void setTemperature(float temperature)
    {
        this.temperature = temperature;
    }

    public float getApparentTemperature()
    {
        return apparentTemperature;
    }

    public void setApparentTemperature(float apparentTemperature)
    {
        this.apparentTemperature = apparentTemperature;
    }

    public float getDewPoint()
    {
        return dewPoint;
    }

    public void setDewPoint(float dewPoint)
    {
        this.dewPoint = dewPoint;
    }

    public float getHumidity()
    {
        return humidity;
    }

    public void setHumidity(float humidity)
    {
        this.humidity = humidity;
    }

    public float getPressure()
    {
        return pressure;
    }

    public void setPressure(float pressure)
    {
        this.pressure = pressure;
    }

    public float getWindSpeed()
    {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed)
    {
        this.windSpeed = windSpeed;
    }

    public float getWindGust()
    {
        return windGust;
    }

    public void setWindGust(float windGust)
    {
        this.windGust = windGust;
    }

    public int getWindBearing()
    {
        return windBearing;
    }

    public void setWindBearing(int windBearing)
    {
        this.windBearing = windBearing;
    }

    public float getCloudCover()
    {
        return cloudCover;
    }

    public void setCloudCover(float cloudCover)
    {
        this.cloudCover = cloudCover;
    }

    public int getUvIndex()
    {
        return uvIndex;
    }

    public void setUvIndex(int uvIndex)
    {
        this.uvIndex = uvIndex;
    }

    public float getVisibility()
    {
        return visibility;
    }

    public void setVisibility(float visibility)
    {
        this.visibility = visibility;
    }

    public float getOzone()
    {
        return ozone;
    }

    public void setOzone(float ozone)
    {
        this.ozone = ozone;
    }

    public long getSunriseTime()
    {
        return sunriseTime;
    }

    public void setSunriseTime(long sunriseTime)
    {
        this.sunriseTime = sunriseTime;
    }

    public long getSunsetTime()
    {
        return sunsetTime;
    }

    public void setSunsetTime(long sunsetTime)
    {
        this.sunsetTime = sunsetTime;
    }

    public float getMoonPhase()
    {
        return moonPhase;
    }

    public void setMoonPhase(float moonPhase)
    {
        this.moonPhase = moonPhase;
    }

    public float getPrecipIntensityMax()
    {
        return precipIntensityMax;
    }

    public void setPrecipIntensityMax(float precipIntensityMax)
    {
        this.precipIntensityMax = precipIntensityMax;
    }

    public float getTemperatureHigh()
    {
        return temperatureHigh;
    }

    public void setTemperatureHigh(float temperatureHigh)
    {
        this.temperatureHigh = temperatureHigh;
    }

    public long getTemperatureHighTime()
    {
        return temperatureHighTime;
    }

    public void setTemperatureHighTime(long temperatureHighTime)
    {
        this.temperatureHighTime = temperatureHighTime;
    }

    public float getTemperatureLow()
    {
        return temperatureLow;
    }

    public void setTemperatureLow(float temperatureLow)
    {
        this.temperatureLow = temperatureLow;
    }

    public long getTemperatureLowTime()
    {
        return temperatureLowTime;
    }

    public void setTemperatureLowTime(long temperatureLowTime)
    {
        this.temperatureLowTime = temperatureLowTime;
    }

    public float getApparentTemperatureHigh()
    {
        return apparentTemperatureHigh;
    }

    public void setApparentTemperatureHigh(float apparentTemperatureHigh)
    {
        this.apparentTemperatureHigh = apparentTemperatureHigh;
    }

    public long getApparentTemperatureHighTime()
    {
        return apparentTemperatureHighTime;
    }

    public void setApparentTemperatureHighTime(long apparentTemperatureHighTime)
    {
        this.apparentTemperatureHighTime = apparentTemperatureHighTime;
    }

    public float getApparentTemperatureLow()
    {
        return apparentTemperatureLow;
    }

    public void setApparentTemperatureLow(float apparentTemperatureLow)
    {
        this.apparentTemperatureLow = apparentTemperatureLow;
    }

    public long getApparentTemperatureLowTime()
    {
        return apparentTemperatureLowTime;
    }

    public void setApparentTemperatureLowTime(long apparentTemperatureLowTime)
    {
        this.apparentTemperatureLowTime = apparentTemperatureLowTime;
    }

    public long getWindGustTime()
    {
        return windGustTime;
    }

    public void setWindGustTime(long windGustTime)
    {
        this.windGustTime = windGustTime;
    }

    public long getUvIndexTime()
    {
        return uvIndexTime;
    }

    public void setUvIndexTime(long uvIndexTime)
    {
        this.uvIndexTime = uvIndexTime;
    }

    public float getTemperatureMin()
    {
        return temperatureMin;
    }

    public void setTemperatureMin(float temperatureMin)
    {
        this.temperatureMin = temperatureMin;
    }

    public long getTemperatureMinTime()
    {
        return temperatureMinTime;
    }

    public void setTemperatureMinTime(long temperatureMinTime)
    {
        this.temperatureMinTime = temperatureMinTime;
    }

    public float getTemperatureMax()
    {
        return temperatureMax;
    }

    public void setTemperatureMax(float temperatureMax)
    {
        this.temperatureMax = temperatureMax;
    }

    public long getTemperatureMaxTime()
    {
        return temperatureMaxTime;
    }

    public void setTemperatureMaxTime(long temperatureMaxTime)
    {
        this.temperatureMaxTime = temperatureMaxTime;
    }

    public float getApparentTemperatureMin()
    {
        return apparentTemperatureMin;
    }

    public void setApparentTemperatureMin(float apparentTemperatureMin)
    {
        this.apparentTemperatureMin = apparentTemperatureMin;
    }

    public long getApparentTemperatureMinTime()
    {
        return apparentTemperatureMinTime;
    }

    public void setApparentTemperatureMinTime(long apparentTemperatureMinTime)
    {
        this.apparentTemperatureMinTime = apparentTemperatureMinTime;
    }

    public float getApparentTemperatureMax()
    {
        return apparentTemperatureMax;
    }

    public void setApparentTemperatureMax(float apparentTemperatureMax)
    {
        this.apparentTemperatureMax = apparentTemperatureMax;
    }

    public long getApparentTemperatureMaxTime()
    {
        return apparentTemperatureMaxTime;
    }

    public void setApparentTemperatureMaxTime(long apparentTemperatureMaxTime)
    {
        this.apparentTemperatureMaxTime = apparentTemperatureMaxTime;
    }
    
    public String getIcon()
    {
        return icon;
    }
    
    public void setIcon(String icon)
    {
        this.icon = icon;
    }
    
    @Override
    public String toString()
    {
        return "{'apparentTemperature':" + this.apparentTemperature + ",'apparentTemperatureHigh':" + this.apparentTemperatureHigh + ",'apparentTemperatureHighTime':" + this.apparentTemperatureHighTime 
            + ",'apparentTemperatureLow':" + this.apparentTemperatureLow + ",'apparentTemperatureLowTime':" + this.apparentTemperatureLowTime + ",'apparentTemperatureMax':" + this.apparentTemperatureMax 
            + ",'apparentTemperatureMaxTime':" + this.apparentTemperatureMaxTime + ",'apparentTemperatureMin':" + this.apparentTemperatureMin + ",'apparentTemperatureMinTime':" + this.apparentTemperatureMinTime 
            + ",'cloudCover':" + this.cloudCover + ",'dewPoint':" + this.dewPoint + ",'humidity':" + this.humidity + ",'icon':" + this.icon + ",'moonPhase':" + this.moonPhase + ",'ozone':" + this.ozone
            + ",'precipIntensity':" + this.precipIntensity + ",'precipIntensityMax':" + this.precipIntensityMax + ",'precipProbability':" + this.precipProbability + ",'pressure':" + this.pressure
            + ",'summary':" + this.summary + ",'sunriseTime':" + this.sunriseTime + ",'sunsetTime':" + this.sunsetTime + ",'temperature':" + this.temperature + ",'temperatureHigh':" + this.temperatureHigh
            + ",'temperatureHighTime':" + this.temperatureHighTime + ",'temperatureLow':" + this.temperatureLow + ",'temperatureLowTime':" + this.temperatureLowTime + ",'temperatureMax':" + this.temperatureMax 
            + ",'temperatureMaxTime':" + this.temperatureMaxTime + ",'temperatureMin':" + this.temperatureMin + ",'temperatureMinTime':" + this.temperatureMinTime + ",'time':" + this.time 
            + ",'uvIndex':" + this.uvIndex + ",'uvIndexTime':" + this.uvIndexTime + ",'visibility':" + this.visibility + ",'windBearing':" + this.windBearing + ",'windGust':" + this.windGust 
            + ",'windGustTime':" + this.windGustTime + ",'windSpeed':" + this.windSpeed
            + "}";
    }
}
