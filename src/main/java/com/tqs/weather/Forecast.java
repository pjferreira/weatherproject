package com.tqs.weather;

import java.io.Serializable;

public class Forecast implements Serializable
{
    private int days;
    private float latitude;
    private float longitude;
    private String timezone;
    private Currently currently;
    private Hourly hourly;
    private Daily daily;
    
    public int getDays()
    {
        return days;
    }
    
    public void setDays(int days)
    {
        this.days = days;
    }
    
    public float getLatitude()
    {
        return latitude;
    }
    
    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }
    
    public float getLongitude()
    {
        return longitude;
    }
    
    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }
    
    public String getTimezone()
    {
        return timezone;
    }
    
    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }
    
    public Currently getCurrently()
    {
        return currently;
    }
    
    public void setCurrently(Currently currently)
    {
        this.currently = currently;
    }
    
    public Hourly getHourly()
    {
        return hourly;
    }
    
    public void setHourly(Hourly hourly)
    {
        this.hourly = hourly;
    }
    
    public Daily getDaily()
    {
        return daily;
    }
    
    public void setDaily(Daily daily)
    {
        this.daily = daily;
    }
    
    @Override
    public String toString()
    {
        return "'forecast':{'days':" + this.days + ",latitude':" + this.latitude + ",'longitude':" + this.longitude + ",'timezone':" + this.timezone + ",'currently':" + this.currently + ",'hourly':" + this.hourly + ",'daily':" + this.daily + "}";
    }
}
