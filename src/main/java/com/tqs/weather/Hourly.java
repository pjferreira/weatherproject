package com.tqs.weather;

import java.io.Serializable;
import java.util.List;

public class Hourly implements Serializable
{
    private String summary;
    private List<Currently> data;
    
    public String getSummary()
    {
        return this.summary;
    }
    
    public void setSummary(String summary)
    {
        this.summary = summary;
    }
    
    public List<Currently> getData()
    {
        return this.data;
    }
    
    public void setData(List<Currently> data)
    {
        this.data = data;
    }
    
    @Override
    public String toString()
    {
        return "{'summary':" + this.summary + ",'data':" + this.data + "}";
    }
}
