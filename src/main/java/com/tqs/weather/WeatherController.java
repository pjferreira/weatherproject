package com.tqs.weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin(origins = "*")
public class WeatherController
{
    @Autowired
    private WeatherRepository weatherRepo;
    
    private static final String APIKEY = "cadcc6c81e26e1f4ea349bb71d222712";
    RestTemplate restTemplate = new RestTemplate();
    
    @GetMapping("/forecast/{lat},{lon}")
    public Forecast getForecast(@PathVariable(value = "lat") String latitude, @PathVariable(value = "lon") String longitude)
    {
        int days = 7;
        StringBuilder url = new StringBuilder();
        url.append("https://api.darksky.net/forecast/").append(APIKEY).append("/").append(latitude).append(",").append(longitude).append("?units=si");
        ResponseEntity<Forecast> result = restTemplate.getForEntity(url.toString(), Forecast.class);
        
        weatherRepo.incrRequest();
        weatherRepo.setHitsMisses(result.getStatusCodeValue());
        
        Forecast forecast = result.getBody();
        
        forecast.setDays(days);
        weatherRepo.saveForecast(forecast);
        
        return forecast;
    }
    
    @GetMapping("/forecast/{lat},{lon},{time}")
    public Forecast getFutureForecast(@PathVariable(value = "lat") String latitude, @PathVariable(value = "lon") String longitude, @PathVariable(value = "time") long time)
    {
        int days = 7;
        StringBuilder url = new StringBuilder();
        url.append("https://api.darksky.net/forecast/").append(APIKEY).append("/").append(latitude).append(",").append(longitude).append(",").append(time).append("?units=si");
        ResponseEntity<Forecast> result = restTemplate.getForEntity(url.toString(), Forecast.class);
        
        weatherRepo.incrRequest();
        weatherRepo.setHitsMisses(result.getStatusCodeValue());
        
        Forecast forecast = result.getBody();
        
        forecast.setDays(days);
        weatherRepo.saveForecast(forecast);
        
        return forecast;
    }
    
    @GetMapping("/forecast/statistics")
    public String getStatistics()
    {
        return weatherRepo.getStatistics();
    }
}
