package com.tqs.weather;

import java.util.ArrayList;
import java.util.List;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SimpleWeatherRepository implements WeatherRepository
{
    private long nRequests = 0;
    private long hits = 0;
    private long misses = 0;
    private List<Forecast> forecasts;
    
    public SimpleWeatherRepository()
    {
        nRequests = 0;
        hits = 0;
        misses = 0;
        forecasts = new ArrayList<>();
    }
    
    @Override
    public String getNumRequests()
    {
        StringBuilder requests = new StringBuilder();
        
        return requests.append("Number of requests made = ").append(nRequests).toString();
    }
    
    @Override
    public void incrRequest()
    {
        nRequests++;
    }

    @Override
    public String getHitsMisses()
    {
        StringBuilder hitsMisses = new StringBuilder();
        
        return hitsMisses.append("Number of hits = ").append(hits).append(", Number of misses = ").append(misses).toString();
    }
    
    @Override
    public void setHitsMisses(int code)
    {
        if (code == 200)
            hits++;
        else
            misses++;
    }
    
    @Override
    public String getStatistics()
    {
        StringBuilder statistics = new StringBuilder();
        
        return statistics.append("Number of requests made = ").append(nRequests).append(", Number of hits = ").append(hits).append(", Number of misses = ").append(misses).append("; Forecasts made in the last 10 seconds: ").append(forecasts).toString();
    }
    
    @Override
    public String getForecasts()
    {
        StringBuilder lastForecasts = new StringBuilder();
        
        return lastForecasts.append("Forecasts made in the last 10 seconds: ").append(forecasts).toString();
    }
    
    @Override
    public void saveForecast(Forecast forecast)
    {
        forecasts.add(forecast);
    }
    
    @Scheduled(fixedRate = 10000)
    @Override
    public void clearCache()
    {
        forecasts.clear();
    }
}
