package com.tqs.weather;

public interface WeatherRepository
{
    public String getNumRequests();
    
    public void incrRequest();
    
    public String getHitsMisses();
    
    public void setHitsMisses(int code);
    
    public String getStatistics();
    
    public void saveForecast(Forecast forecast);
    
    public String getForecasts();
    
    public void clearCache();
}
