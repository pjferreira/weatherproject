package com.tqs.weather;

import com.tngtech.java.junit.dataprovider.*;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.response.Response;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;

@RunWith(DataProviderRunner.class)
public class WeatherRESTAssuredTest
{
    private static final String URL = "/forecast/{lat},{lon}";
    private static final String URLWITHTIME = "/forecast/{lat},{lon},{time}";
    
    @DataProvider
    public static Object[][] coordinates()
    {
        return new Object[][] {
            { "40.6389", "-8.6553" },
            { "41.1494512", "-8.6107884" }
        };
    }
    
    @DataProvider
    public static Object[][] coordinatesWithTime()
    {
        return new Object[][] {
            { "40.6389", "-8.6553", "1557579600" },
            { "41.1494512", "-8.6107884", "1557579600" }
        };
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void givenUrl_whenSuccessOnGetsResponseAndJsonHasRequiredKV_thenCorrect(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            statusCode(200).
            assertThat().
            body("timezone", equalTo("Europe/Lisbon")); 
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void givenUrlWithTime_whenSuccessOnGetsResponseAndJsonHasRequiredKV_thenCorrect(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            statusCode(200).
            assertThat().
            body("timezone", equalTo("Europe/Lisbon")); 
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void givenUrl_whenSuccessOnGetsResponseAndJsonResponseHasRequiredKVUnderKey_thenCorrect(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            statusCode(200).
            assertThat().
            body("currently.sunriseTime", equalTo(0));
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void givenUrlWithTime_whenSuccessOnGetsResponseAndJsonResponseHasRequiredKVUnderKey_thenCorrect(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            statusCode(200).
            assertThat().
            body("currently.sunriseTime", equalTo(0));
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void givenUrl_whenSuccessOnGetsResponseAndJsonResponseHasArrayWithGivenValuesUnderKey_thenCorrect(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            statusCode(200).
            assertThat().
            body("hourly.data.sunsetTime", hasItems(0));
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void givenUrlWithTime_whenSuccessOnGetsResponseAndJsonResponseHasArrayWithGivenValuesUnderKey_thenCorrect(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            statusCode(200).
            assertThat().
            body("hourly.data.sunsetTime", hasItems(0));
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void whenRequestForecast_thenLatitudeIsFloat(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            assertThat().
            body("latitude", equalTo(Float.parseFloat(lat)));
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenRequestForecast_thenLatitudeIsFloat(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            assertThat().
            body("latitude", equalTo(Float.parseFloat(lat)));
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void whenRequestForecast_thenLongitudeIsFloat(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            assertThat().
            body("longitude", equalTo(Float.parseFloat(lon)));
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenRequestForecastWithTime_thenLongitudeIsFloat(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            assertThat().
            body("longitude", equalTo(Float.parseFloat(lon)));
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void whenRequestGet_thenOK(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            request("GET", URL).
        then().
            statusCode(200);
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenRequestWithTimeGet_thenOK(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            request("GET", URLWITHTIME).
        then().
            statusCode(200);
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void whenMeasureResponseTime_thenOK(String lat, String lon)
    {
        Response response = RestAssured.given().pathParam("lat", lat).pathParam("lon", lon).when().get(URL);
        long timeInMS = response.time();
        long timeInS = response.timeIn(TimeUnit.SECONDS);

        assertEquals(timeInS, timeInMS/1000);
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenMeasureResponseTimeURLWithTime_thenOK(String lat, String lon, String time)
    {
        Response response = RestAssured.given().pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).when().get(URLWITHTIME);
        long timeInMS = response.time();
        long timeInS = response.timeIn(TimeUnit.SECONDS);

        assertEquals(timeInS, timeInMS/1000);
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void whenValidateResponseTime_thenSuccess(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            time(lessThan(5000L));
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenValidateResponseTimeURLWithTime_thenSuccess(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            time(lessThan(5000L));
    }
    
    @Test
    @UseDataProvider("coordinates")
    public void whenLogRequest_thenOK(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
            log().
            all().
        when().
            get(URL).
        then().
            statusCode(200);
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenLogRequestWithTime_thenOK(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
            log().
            all().
        when().
            get(URLWITHTIME).
        then().
            statusCode(200);
    }

    @Test
    @UseDataProvider("coordinates")
    public void whenLogResponse_thenOK(String lat, String lon)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).
        when().
            get(URL).
        then().
            log().
            body().
            statusCode(200);
    }
    
    @Test
    @UseDataProvider("coordinatesWithTime")
    public void whenLogResponseWithTime_thenOK(String lat, String lon, String time)
    {
        given().
            pathParam("lat", lat).pathParam("lon", lon).pathParam("time", time).
        when().
            get(URLWITHTIME).
        then().
            log().
            body().
            statusCode(200);
    }
}
