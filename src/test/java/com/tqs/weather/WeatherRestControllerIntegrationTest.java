package com.tqs.weather;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherController.class)
public class WeatherRestControllerIntegrationTest
{
    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private WeatherRepository weatherRepo;
    
    private RestTemplate restTemplate = new RestTemplate();
    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();
    
    @Before
    public void init()
    {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }
    
    @Test
    public void givenForecast_whenGetForecast_thenReturnJson() throws Exception
    {
        Forecast forecast = new Forecast();
        
        mockServer.expect(ExpectedCount.once(), 
          requestTo(new URI("http://localhost:8080/forecast/40.6389,-8.6553")))
          .andExpect(method(HttpMethod.GET))
          .andRespond(withStatus(HttpStatus.OK)
          .contentType(MediaType.APPLICATION_JSON)
          .body(mapper.writeValueAsString(forecast))
        );
        
        Forecast fore = restTemplate.getForEntity("http://localhost:8080/forecast/40.6389,-8.6553", Forecast.class).getBody();
        mockServer.verify();
        assertThat(forecast).isEqualToComparingFieldByField(fore);
    }
    
    @Test
    public void givenForecast_whenGetFutureForecast_thenReturnJson() throws Exception
    {
        Forecast forecast = new Forecast();
        
        mockServer.expect(ExpectedCount.once(), 
          requestTo(new URI("http://localhost:8080/forecast/40.6389,-8.6553,1557702000")))
          .andExpect(method(HttpMethod.GET))
          .andRespond(withStatus(HttpStatus.OK)
          .contentType(MediaType.APPLICATION_JSON)
          .body(mapper.writeValueAsString(forecast))
        );
        
        Forecast fore = restTemplate.getForEntity("http://localhost:8080/forecast/40.6389,-8.6553,1557702000", Forecast.class).getBody();
        mockServer.verify();
        assertThat(forecast).isEqualToComparingFieldByField(fore);
    }
    
    @Test
    public void givenString_whenGetStatistics_ThenReturnString() throws Exception
    {
        String expectedStatistics = "Number of requests made = 0, Number of hits = 0, Number of misses = 0; Forecasts made in the last 10 seconds: []";
        
        given(weatherRepo.getStatistics()).willReturn(expectedStatistics);
        
        mvc.perform(MockMvcRequestBuilders.get("/forecast/statistics")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().string(equalTo("Number of requests made = 0, Number of hits = 0, Number of misses = 0; Forecasts made in the last 10 seconds: []")));
    }
}
