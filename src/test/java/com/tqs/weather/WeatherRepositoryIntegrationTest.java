package com.tqs.weather;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherRepository.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WeatherRepositoryIntegrationTest
{
    @Autowired
    private WeatherRepository weatherRepo;
    
    @Test
    public void AWhenGetNumberRequets_thenReturnTotalNumberRequestsMade()
    {
        //given
        String expectedNumRequestsMade = "Number of requests made = 0";
        
        //when
        String numRequestsMade = weatherRepo.getNumRequests();
        
        //then
        assertThat(numRequestsMade).isEqualTo(expectedNumRequestsMade);
    }
    
    @Test
    public void BWhenGetStatistics_thenReturnAllStatisticsAboutCache()
    {
        //given
        String expectedStatistics = "Number of requests made = 0, Number of hits = 0, Number of misses = 0; Forecasts made in the last 10 seconds: []";
        
        //when
        String statistics = weatherRepo.getStatistics();
        
        //then
        assertThat(statistics).isEqualTo(expectedStatistics);
    }
    
    @Test
    public void CWhenIncrementNumberRequest_thenTotalNumberRequestsMadeIncremented()
    {
        //given
        String expectedNumRequestsMade = "Number of requests made = 2";
        
        //when
        weatherRepo.incrRequest();
        weatherRepo.incrRequest();
        String numRequestsMade = weatherRepo.getNumRequests();
        
        //then
        assertThat(numRequestsMade).isEqualTo(expectedNumRequestsMade);
    }
    
    @Test
    public void DWhenGetHitsMisses_thenReturnTotalNumberHitsMissesMade()
    {
        //given
        String expectedNumHitsMissesMade = "Number of hits = 0, Number of misses = 0";
        
        //when
        String numHitsMissesMade = weatherRepo.getHitsMisses();
        
        //then
        assertThat(numHitsMissesMade).isEqualTo(expectedNumHitsMissesMade);
    }
    
    @Test
    public void EWhenSetHitsMisses_thenTotalNumberHitsMissesMadeIncremented()
    {
        //given
        String expectedNumHitsMissesMade = "Number of hits = 2, Number of misses = 1";
        
        //when
        weatherRepo.setHitsMisses(200);
        weatherRepo.setHitsMisses(200);
        weatherRepo.setHitsMisses(100);
        String numHitsMissesMade = weatherRepo.getHitsMisses();
        
        //then
        assertThat(numHitsMissesMade).isEqualTo(expectedNumHitsMissesMade);
    }
    
    @Test
    public void FWhenGetForecasts_thenReturnAllForecastsRequestedInLast10Seconds()
    {
        //given
        String expectedForecasts = "Forecasts made in the last 10 seconds: []";
        
        //when
        String forecasts = weatherRepo.getForecasts();
        
        //then
        assertThat(forecasts).isEqualTo(expectedForecasts);
    }
    
    @Test
    public void GWhenSaveForecast_thenForecastIsSaved()
    {
        //given
        String expectedForecasts = "Forecasts made in the last 10 seconds: ['forecast':{'days':0,latitude':0.0,'longitude':0.0,'timezone':null,'currently':null,'hourly':null,'daily':null}]";
        
        //when
        Forecast forecast = new Forecast();
        weatherRepo.saveForecast(forecast);
        String forecasts = weatherRepo.getForecasts();
        
        //then
        assertThat(forecasts).isEqualTo(expectedForecasts);
    }
    
    @Test
    public void HWhenClearCache_thenAllForecastsRequestedInLast10SecondsAreDeleted()
    {
        //given
        String expectedForecasts = "Forecasts made in the last 10 seconds: []";
        
        //when
        Forecast forecast = new Forecast();
        weatherRepo.saveForecast(forecast);
        weatherRepo.clearCache();
        String forecasts = weatherRepo.getForecasts();
        
        //then
        assertThat(forecasts).isEqualTo(expectedForecasts);
    }
}
